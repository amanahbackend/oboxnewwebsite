﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;

namespace AspNetCoreMvcTemplate.Services
{
    /// <summary>
    /// Workaround for parsing issue https://github.com/aspnet/AspNetCore/issues/11262
    /// </summary>
    public class FormWorkaroundMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly FormOptions _formOptions;

        public FormWorkaroundMiddleware(RequestDelegate next, IOptions<FormOptions> options)
        {
            _next = next;
            _formOptions = options.Value;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.HasFormContentType)
            {
                var previousFormFeature = httpContext.Features.Get<IFormFeature>();
                httpContext.Features.Set<IFormFeature>(new WorkaroundFormFeature(httpContext.Request, previousFormFeature, _formOptions));
            }

            await _next(httpContext);
        }

        private class WorkaroundFormFeature : IFormFeature
        {
            private readonly HttpRequest _request;
            private readonly IFormFeature _feature;
            private readonly FormOptions _options;

            public WorkaroundFormFeature(HttpRequest request, IFormFeature feature, FormOptions options)
            {
                _request = request;
                _feature = feature;
                _options = options;
            }

            private MediaTypeHeaderValue ContentType
            {
                get
                {
                    MediaTypeHeaderValue.TryParse(_request.ContentType, out MediaTypeHeaderValue mt);
                    return mt;
                }
            }

            private bool HasApplicationFormContentType =>
                ContentType?.MediaType.Equals("application/x-www-form-urlencoded", StringComparison.OrdinalIgnoreCase) == true;

            public bool HasFormContentType => _feature.HasFormContentType;

            public IFormCollection Form { get => _feature.Form; set => _feature.Form = value; }

            public IFormCollection ReadForm()
            {
                if (Form != null)
                {
                    return Form;
                }

                if (HasApplicationFormContentType)
                {
                    var encoding = FilterEncoding(ContentType.Encoding);
                    var formReader = new FormReader(_request.Body, encoding)
                    {
                        ValueCountLimit = _options.ValueCountLimit,
                        KeyLengthLimit = _options.KeyLengthLimit,
                        ValueLengthLimit = _options.ValueLengthLimit,
                    };
                    return Form = new FormCollection(formReader.ReadForm());
                }

                return _feature.ReadForm();
            }

            public async Task<IFormCollection> ReadFormAsync(CancellationToken cancellationToken)
            {
                if (Form != null)
                {
                    return Form;
                }

                if (HasApplicationFormContentType)
                {
                    var encoding = FilterEncoding(ContentType.Encoding);
                    var formReader = new FormReader(_request.Body, encoding)
                    {
                        ValueCountLimit = _options.ValueCountLimit,
                        KeyLengthLimit = _options.KeyLengthLimit,
                        ValueLengthLimit = _options.ValueLengthLimit,
                    };
                    return Form = new FormCollection(await formReader.ReadFormAsync());
                }
                return await _feature.ReadFormAsync(cancellationToken);
            }

            private Encoding FilterEncoding(Encoding encoding)
            {
                // UTF-7 is insecure and should not be honored. UTF-8 will succeed for most cases.
                if (encoding == null || Encoding.UTF7.Equals(encoding))
                {
                    return Encoding.UTF8;
                }
                return encoding;
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class FormWorkaroundMiddlewareExtensions
    {
        public static IApplicationBuilder UseFormWorkaroundMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<FormWorkaroundMiddleware>();
        }
    }
}



