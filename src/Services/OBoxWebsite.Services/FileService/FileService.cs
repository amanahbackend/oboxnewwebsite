﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

using System.IO;
using static System.Net.Mime.MediaTypeNames;
using Microsoft.AspNetCore.Hosting;

namespace AspNetCoreMvcTemplate.Services.FileService
{
    public partial class FileService : IFileService
    {

        IHostingEnvironment hostingEnvironment;

        public FileService(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
        }
        /// <param name="base64FileType">"the first prefix of base64 .. the type of file sent data:image/"</param>
        /// <param name="base64string">the base 64 string as the javascript reader created</param>
        public string SaveFile(string base64FileType, string base64string)
        {
            //var base64array = Convert.FromBase64String(base64string);
            //var filePath = Path.Combine($"{Environment.ContentRootPath}/wwwroot/img/user/{Guid.NewGuid()}.jpg");
            //var filePath = Path.Combine($"{}//{Guid.NewGuid()}.jpg");
            //System.IO.File.WriteAllBytes(filePath, base64array);
            string base64Spliter = ";base64,";
            int indexOfbase64 = base64string.IndexOf(base64Spliter);
            string extention = base64string.Substring(base64FileType.Length, (indexOfbase64 - base64FileType.Length));

            indexOfbase64 += base64Spliter.Length;
            base64string = base64string.Substring(indexOfbase64, base64string.Length - indexOfbase64);
            var base64array = Convert.FromBase64String(base64string);


            string result = $"{Guid.NewGuid()}.{extention}";
            string fileDirectory = Path.Combine($"{hostingEnvironment.WebRootPath}/uploads");

            if (!Directory.Exists(fileDirectory))
                Directory.CreateDirectory(fileDirectory);

            var filePath = $"{fileDirectory}/{result}";
            System.IO.File.WriteAllBytes(filePath, base64array);


            return result;

        }
        /// <param name="extention">"the file extention"</param>
        /// <param name="base64string">the base 64 string as the javascript reader created</param>
        /// <param name="fileModelType">The Model Type That Will contain file like Employee </param>
        public string SaveFileByExtention(string extention, string base64string)
        {
            //var base64array = Convert.FromBase64String(base64string);
            //var filePath = Path.Combine($"{Environment.ContentRootPath}/wwwroot/img/user/{Guid.NewGuid()}.jpg");
            //var filePath = Path.Combine($"{}//{Guid.NewGuid()}.jpg");
            //System.IO.File.WriteAllBytes(filePath, base64array);
            string base64Spliter = ";base64,";
            int indexOfbase64 = base64string.IndexOf(base64Spliter);

            indexOfbase64 += base64Spliter.Length;
            base64string = base64string.Substring(indexOfbase64, base64string.Length - indexOfbase64);
            var base64array = Convert.FromBase64String(base64string);

            string result = $"{Guid.NewGuid()}.{extention}";
            string fileDirectory = Path.Combine($"{hostingEnvironment.WebRootPath}/uploads");

            if (!Directory.Exists(fileDirectory))
                Directory.CreateDirectory(fileDirectory);

            var filePath = $"{fileDirectory}/{result}";
            System.IO.File.WriteAllBytes(filePath, base64array);

            return result;
        }

        public bool IsBase64(string imageAsBase64)
        {
            return imageAsBase64.Contains("data:");
        }

        public string ImageUrlrl(string base64OrUrl)
        {
            if (string.IsNullOrEmpty(base64OrUrl)) return null;
            
            if (IsBase64(base64OrUrl))
            {
                string url = SaveFile("data:image/", base64OrUrl);
                return $@"\uploads\{url}";
            }
            return base64OrUrl;
        }

    }
    //public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
    //    {
    //        MemoryStream ms = new MemoryStream();
    //        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
    //        return ms.ToArray();
    //    }

    //    public static Image ByteArrayToImage(byte[] byteArrayIn)
    //    {
    //        MemoryStream ms = new MemoryStream(byteArrayIn);
    //        Image returnImage = Image.FromStream(ms);
    //        return returnImage;
    //    }



}
