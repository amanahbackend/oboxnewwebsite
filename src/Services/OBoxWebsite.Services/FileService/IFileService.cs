﻿
using System;
using System.Collections.Generic;

namespace AspNetCoreMvcTemplate.Services.FileService
{
    /// <summary>
    /// File service interface
    /// </summary>
    public partial interface IFileService 
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="base64FileType">"the first prefix of base64 .. the type of file sent data:image/"</param>
        /// <param name="base64string">the base 64 string as the javascript reader created</param>
        /// <returns></returns>
        string SaveFile(string base64FileType, string base64string);
        string SaveFileByExtention(string extention, string base64string);

        bool IsBase64(string imageAsBase64);

        string ImageUrlrl(string base64OrUrl);
    }
}