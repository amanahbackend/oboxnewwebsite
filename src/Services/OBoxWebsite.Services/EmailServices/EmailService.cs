﻿
using AspNetCoreMvcTemplate.Data.Models.Base;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCoreMvcTemplate.Services.EmailServices
{
    public class EmailService : IEmailService
    {
        private IHostingEnvironment _hostingEnvironment;

        public EmailService(IConfiguration configuration, IHostingEnvironment _hostingEnvironment)
        {
            Configuration = configuration;
            this._hostingEnvironment = _hostingEnvironment;
        }
        public static IConfiguration Configuration { get; set; }
        public async Task SendEmailAsync(IEmailMassage emailMassage)
        {
            var MailServer = Configuration["EmailSettings:MailServer"];
            var Password = Configuration["EmailSettings:Password"];
            var Sender = Configuration["EmailSettings:Sender"];
            var MailPort = int.Parse(Configuration["EmailSettings:MailPort"]);
            var SenderName = Configuration["EmailSettings:SenderName"];
            var IsEnableSSL = bool.Parse(Configuration["EmailSettings:IsEnableSSL"]);

            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(SenderName);
            emailMassage.MailTo.ForEach(email => mail.To.Add(email));

            mail.IsBodyHtml = true;
            string htmlBody = $"Email Title{emailMassage.Title},Email Subtitle{emailMassage.SubTitle},Email Title{emailMassage.Body}";

           
            mail.Subject = emailMassage.Subject;
            mail.Body = htmlBody;
            using (SmtpClient smtpServer = new SmtpClient())
            {
                smtpServer.UseDefaultCredentials = false;
                smtpServer.Credentials = new NetworkCredential(Sender, Password);
                smtpServer.Host = MailServer;
                smtpServer.Port = MailPort;
                smtpServer.EnableSsl = IsEnableSSL;

                await smtpServer.SendMailAsync(mail);
            }
        }
    }
}
