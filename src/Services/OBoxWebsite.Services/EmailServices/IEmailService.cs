﻿
using AspNetCoreMvcTemplate.Data.Models.Base;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCoreMvcTemplate.Services.EmailServices
{
    public interface IEmailService
    {
          Task SendEmailAsync(IEmailMassage emailMassage);
    }
}
