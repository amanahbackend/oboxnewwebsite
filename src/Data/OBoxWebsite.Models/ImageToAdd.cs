﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class ImageToAdd: BaseModelViewable
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
    }
}
