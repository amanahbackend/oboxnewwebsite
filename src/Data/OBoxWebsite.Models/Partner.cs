﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class Partner : BaseModelViewable
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public string Image { get; set; }

        public int? Order { get; set; }
    }
}
