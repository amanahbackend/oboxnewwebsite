﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class SubscribersMail : IEmailMassage
    {
        [Key]
        public int Id { get; set; }
        public DateTime SentDate { get; set; }

        [NotMapped]
        public List<string> MailTo { get; set; }

        public string Subject { get; set; }
        public string Body { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
    }
}
