﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class SubscriberModel
    {
        [Key]
        public int Id { get; set; }

        public string Email { get; set; }
        public DateTime Createdate { get; set; }

    }
}
