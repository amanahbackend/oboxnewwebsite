﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class TeamMember : BaseModelViewable
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Image { get; set; }
        public string FirstLink { get; set; }
        public string SecondLink { get; set; }
        
        public int? Order { get; set; }
        public DateTime Createdate { get; set; }

    }
}
