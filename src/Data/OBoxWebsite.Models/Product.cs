﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class Product : BaseModelViewable
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string CssClass { get; set; }
        public int Order { get; set; }

        public string PageIcon { get; set; }
        public string PageGreatingImg { get; set; }

        public string PageGreatingHTML { get; set; }
        public string PageFocusHeaderHTML { get; set; }
        public string PageLastSectionHTML { get; set; }



        public string PageLastSectionImage { get; set; }

        [MaxLength(2500000)]
        public string ProductFooterHTML { get; set; }
        public bool ViewInWebSiteProductFooterHTML { get; set; }
        

        public ICollection<ProductFocusItem> ProductFocusItems { get; set; }

        //public ICollection<ProductHint> ProductHints { get; set; }

    }

}