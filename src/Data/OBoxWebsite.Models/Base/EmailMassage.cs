﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCoreMvcTemplate.Data.Models.Base
{
    public interface IEmailMassage
    {
        List<string> MailTo { get; set; }
        string Subject { get; set; }
        string Body { get; set; }
        string Title { get; set; }
        string SubTitle { get; set; }
    }
}
