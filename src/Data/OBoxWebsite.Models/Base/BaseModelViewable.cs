﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCoreMvcTemplate.Data.Models.Base
{
    public abstract class BaseModelViewable
    {
        public bool IsViewInWebSite { get; set; }
    }
}
