﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class AboutUsCollapsible : BaseModelViewable
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int? Order { get; set; }
    }
}
