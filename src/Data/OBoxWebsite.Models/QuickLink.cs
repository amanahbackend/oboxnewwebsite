﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class QuickLink : BaseModelViewable
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }

        public int? Order { get; set; }
    }
}
