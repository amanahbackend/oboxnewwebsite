﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class SlideItem :  BaseModelViewable
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Image { get; set; }

        [MaxLength(5000)]
        public string Description { get; set; }

        public bool HasDivOnBackGround { get; set; }

        public bool IsVideo { get; set; }

        public string BackGroundUrl { get; set; }

        public int? Order { get; set; }
    }
}
