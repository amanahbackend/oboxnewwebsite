﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class AboutUsTitle : BaseModelViewable
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }

        public int? Order { get; set; }
    }
}
