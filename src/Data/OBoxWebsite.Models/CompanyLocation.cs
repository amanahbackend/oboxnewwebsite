﻿using AspNetCoreMvcTemplate.Data.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class CompanyLocation : BaseModelViewable
    {
        [Key]
        public int Id { get; set; }

        public string Email { get; set; }
        public string LocationName { get; set; }
        public string LocationAddress { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string ForSalesPhones { get; set; }
        public string ForServiceAndSupportPhones { get; set; }
        public string MapDivId { get; set; }
        public bool IsDefaultLocation { get; set; }


        [MaxLength(1000)]
        public string ScriptForMap { get; set; }

        public int? Order { get; set; }

    }
}
