﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AspNetCoreMvcTemplate.Data.Models
{
    public class AboutUs
    {
        [Key]
        public int Id { get; set; }
        //public string HtmlValues { get; set; }

        public string PageGreatingImg { get; set; }
        public string PageGreatingHTML { get; set; }

        public string PageLastSectionHTML { get; set; }
        public string PageLastSectionImage { get; set; }
    }
}
