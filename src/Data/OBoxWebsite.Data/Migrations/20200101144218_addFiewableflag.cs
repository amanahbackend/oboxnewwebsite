﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class addFiewableflag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsViewInWebSite",
                table: "SlideItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsViewInWebSite",
                table: "Products",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsViewInWebSite",
                table: "Partners",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsViewInWebSite",
                table: "ImageToAdds",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsViewInWebSite",
                table: "BoxContainers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsViewInWebSite",
                table: "SlideItems");

            migrationBuilder.DropColumn(
                name: "IsViewInWebSite",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "IsViewInWebSite",
                table: "Partners");

            migrationBuilder.DropColumn(
                name: "IsViewInWebSite",
                table: "ImageToAdds");

            migrationBuilder.DropColumn(
                name: "IsViewInWebSite",
                table: "BoxContainers");
        }
    }
}
