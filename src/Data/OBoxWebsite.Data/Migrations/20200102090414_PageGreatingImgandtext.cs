﻿
using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class PageGreatingImgandtext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PageGreatingImg",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PageGreatingText",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PageGreatingImg",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "PageGreatingText",
                table: "Products");
        }
    }
}
