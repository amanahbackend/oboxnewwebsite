﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class addIsAVideotoslide : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsAVideo",
                table: "SlideItems",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAVideo",
                table: "SlideItems");
        }
    }
}
