﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class add_ProductFooterHTML : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProductFooterHTML",
                table: "Products",
                maxLength: 2147483647,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ViewInWebSiteProductFooterHTML",
                table: "Products",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductFooterHTML",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ViewInWebSiteProductFooterHTML",
                table: "Products");
        }
    }
}
