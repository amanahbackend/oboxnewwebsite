﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class MailToSubscriberModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SocialMediaLInks",
                table: "SocialMediaLInks");

            migrationBuilder.RenameTable(
                name: "SocialMediaLInks",
                newName: "SocialMediaLinks");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SocialMediaLinks",
                table: "SocialMediaLinks",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "MailToSubscribers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SentDate = table.Column<DateTime>(nullable: false),
                    Subject = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    SubTitle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailToSubscribers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MailToSubscribers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SocialMediaLinks",
                table: "SocialMediaLinks");

            migrationBuilder.RenameTable(
                name: "SocialMediaLinks",
                newName: "SocialMediaLInks");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SocialMediaLInks",
                table: "SocialMediaLInks",
                column: "Id");
        }
    }
}
