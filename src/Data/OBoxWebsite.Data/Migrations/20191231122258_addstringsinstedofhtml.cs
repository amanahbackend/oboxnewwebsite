﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class addstringsinstedofhtml : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Html",
                table: "SlideItems",
                newName: "Description");

            migrationBuilder.AddColumn<string>(
                name: "SubTitle",
                table: "SlideItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "SlideItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubTitle",
                table: "SlideItems");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "SlideItems");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "SlideItems",
                newName: "Html");
        }
    }
}
