﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class somechanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductFocusItem_Products_ProductId",
                table: "ProductFocusItem");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductHint_Products_ProductId",
                table: "ProductHint");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductHint",
                table: "ProductHint");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductFocusItem",
                table: "ProductFocusItem");

            migrationBuilder.RenameTable(
                name: "ProductHint",
                newName: "ProductHints");

            migrationBuilder.RenameTable(
                name: "ProductFocusItem",
                newName: "ProductFocusItems");

            migrationBuilder.RenameIndex(
                name: "IX_ProductHint_ProductId",
                table: "ProductHints",
                newName: "IX_ProductHints_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductFocusItem_ProductId",
                table: "ProductFocusItems",
                newName: "IX_ProductFocusItems_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductHints",
                table: "ProductHints",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductFocusItems",
                table: "ProductFocusItems",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductFocusItems_Products_ProductId",
                table: "ProductFocusItems",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductHints_Products_ProductId",
                table: "ProductHints",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductFocusItems_Products_ProductId",
                table: "ProductFocusItems");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductHints_Products_ProductId",
                table: "ProductHints");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductHints",
                table: "ProductHints");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductFocusItems",
                table: "ProductFocusItems");

            migrationBuilder.RenameTable(
                name: "ProductHints",
                newName: "ProductHint");

            migrationBuilder.RenameTable(
                name: "ProductFocusItems",
                newName: "ProductFocusItem");

            migrationBuilder.RenameIndex(
                name: "IX_ProductHints_ProductId",
                table: "ProductHint",
                newName: "IX_ProductHint_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductFocusItems_ProductId",
                table: "ProductFocusItem",
                newName: "IX_ProductFocusItem_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductHint",
                table: "ProductHint",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductFocusItem",
                table: "ProductFocusItem",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductFocusItem_Products_ProductId",
                table: "ProductFocusItem",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductHint_Products_ProductId",
                table: "ProductHint",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
