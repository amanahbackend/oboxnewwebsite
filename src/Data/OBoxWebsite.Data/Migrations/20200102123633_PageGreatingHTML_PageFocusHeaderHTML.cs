﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class PageGreatingHTML_PageFocusHeaderHTML : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PageGreatingText",
                table: "Products",
                newName: "PageGreatingHTML");

            migrationBuilder.AddColumn<string>(
                name: "PageFocusHeaderHTML",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ProductFocusItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsViewInWebSite = table.Column<bool>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductFocusItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductFocusItem_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductHint",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsViewInWebSite = table.Column<bool>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Html = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductHint", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductHint_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductFocusItem_ProductId",
                table: "ProductFocusItem",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductHint_ProductId",
                table: "ProductHint",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductFocusItem");

            migrationBuilder.DropTable(
                name: "ProductHint");

            migrationBuilder.DropColumn(
                name: "PageFocusHeaderHTML",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "PageGreatingHTML",
                table: "Products",
                newName: "PageGreatingText");
        }
    }
}
