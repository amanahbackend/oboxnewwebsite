﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class somefixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HtmlValues",
                table: "AboutUss");

            migrationBuilder.RenameColumn(
                name: "Html",
                table: "AboutUsTitles",
                newName: "Title");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "AboutUsTitles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "AboutUsTitles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "AboutUsTitles");

            migrationBuilder.DropColumn(
                name: "Image",
                table: "AboutUsTitles");

            migrationBuilder.RenameColumn(
                name: "Title",
                table: "AboutUsTitles",
                newName: "Html");

            migrationBuilder.AddColumn<string>(
                name: "HtmlValues",
                table: "AboutUss",
                nullable: true);
        }
    }
}
