﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class fix_AboutUs_remove_productHints_Add_AboutUsTitle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductHints");

            migrationBuilder.AddColumn<string>(
                name: "PageGreatingHTML",
                table: "AboutUss",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PageGreatingImg",
                table: "AboutUss",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PageLastSectionHTML",
                table: "AboutUss",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PageLastSectionImage",
                table: "AboutUss",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AboutUsTitles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsViewInWebSite = table.Column<bool>(nullable: false),
                    Html = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AboutUsTitles", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AboutUsTitles");

            migrationBuilder.DropColumn(
                name: "PageGreatingHTML",
                table: "AboutUss");

            migrationBuilder.DropColumn(
                name: "PageGreatingImg",
                table: "AboutUss");

            migrationBuilder.DropColumn(
                name: "PageLastSectionHTML",
                table: "AboutUss");

            migrationBuilder.DropColumn(
                name: "PageLastSectionImage",
                table: "AboutUss");

            migrationBuilder.CreateTable(
                name: "ProductHints",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Html = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    IsViewInWebSite = table.Column<bool>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductHints", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductHints_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductHints_ProductId",
                table: "ProductHints",
                column: "ProductId");
        }
    }
}
