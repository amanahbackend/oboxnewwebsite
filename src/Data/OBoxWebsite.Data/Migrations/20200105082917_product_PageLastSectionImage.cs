﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AspNetCoreMvcTemplate.Data.Migrations
{
    public partial class product_PageLastSectionImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PageLastSectionImage",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PageLastSectionImage",
                table: "Products");
        }
    }
}
