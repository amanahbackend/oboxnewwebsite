﻿namespace AspNetCoreMvcTemplate.Data
{
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }




        public DbSet<ImageToAdd> ImageToAdds { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<SlideItem> SlideItems { get; set; }
        public DbSet<BoxContainer> BoxContainers { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<ProductFocusItem> ProductFocusItems { get; set; }
        public DbSet<ContactModel> ContactModels { get; set; }
        public DbSet<SubscriberModel> SubscriberModels { get; set; }
        public DbSet<CompanyLocation> CompanyLocations { get; set; }
        public DbSet<SocialMediaLink> SocialMediaLinks { get; set; }

        

        public DbSet<AboutUs> AboutUss { get; set; }

        public DbSet<AboutUsCollapsible> AboutUsCollapsibles { get; set; }


        
        public DbSet<AboutUsTitle> AboutUsTitles { get; set; }
        public DbSet<TeamMember> TeamMembers { get; set; }
        public DbSet<QuickLink> QuickLinks { get; set; }

        public DbSet<ApplicationUser> users { get; set; }
        public DbSet<SubscribersMail> MailToSubscribers { get; set; }
    }
}