﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using AspNetCoreMvcTemplate.Web.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreMvcTemplate.Web.Controllers
{
    [ApiController]
    //[Route("api/Contact")]
    public class ContactController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        public ContactController(ApplicationDbContext context)
        {
            this._context = context;
        }
        // POST: Admin/ContactModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("api/Contact/Create")]
        public async Task<bool> Create(ContactViewModel contactModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(new ContactModel()
                {
                    Createdate = DateTime.UtcNow,
                    Email = contactModel.Email,
                    Message = contactModel.Message,
                    Phone = contactModel.Phone,
                    Subject = contactModel.Subject
                });
                int result = await _context.SaveChangesAsync();
                //return Redirect("/");
                return result > 0;
            }
            return false;
        }



        // POST: Admin/ContactModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("api/Contact/CreateSubscriber")]
        public async Task<bool> CreateSubscriber(SubscriberViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isExist = _context.SubscriberModels.Any(e => e.Email.Trim().ToLower() == model.Email.Trim().ToLower());

                if (isExist) return true;

                _context.Add(new SubscriberModel()
                {
                    Createdate = DateTime.UtcNow,
                    Email = model.Email,
                });

                int result = await _context.SaveChangesAsync();
                //return Redirect("/");
                return result > 0;
            }
            return false;
        }



    }
}