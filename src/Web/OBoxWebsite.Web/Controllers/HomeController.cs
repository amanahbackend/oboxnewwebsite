﻿using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace AspNetCoreMvcTemplate.Web.Controllers
{
    using AspNetCoreMvcTemplate.Data;
    using AspNetCoreMvcTemplate.Data.Models;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using ViewModels;


    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _hostingEnv;

        public HomeController(ApplicationDbContext context, IHostingEnvironment hostingEnv)
        {
            this._context = context;
            this._hostingEnv = hostingEnv; 
        }
        public IActionResult Index()
        {
            var vm = new IndexVM()
            {
                Products = _context.Products.Where(x => x.IsViewInWebSite).OrderBy(x => x.Order).ToList(),
                BoxContainers = _context.BoxContainers.Where(x => x.IsViewInWebSite).OrderBy(x => x.Order).ToList(),
                Partners = _context.Partners.Where(x => x.IsViewInWebSite).OrderBy(x => x.Order).ToList(),
                SlideItems = _context.SlideItems.Where(x => x.IsViewInWebSite).OrderBy(x => x.Order).ToList(),
            };
            return this.View(vm);
        }

        public IActionResult Privacy()
        {
            return this.View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        // GET: Products/5
        public async Task<IActionResult> Product(int id)
        {
            if (id == 0)
                RedirectToAction("index");
            Product product;
            product = await _context.Products
                //.Include(x => x.ProductHints)
                .Include(x => x.ProductFocusItems)
                .FirstOrDefaultAsync(x => x.IsViewInWebSite && x.Id == id);

            //product.PageGreatingImg = Path.Combine(hostingEnv.WebRootPath, ChampionsImageFolder)
            product.PageGreatingImg =$"{this.Request.Scheme}://{this.Request.Host.Value}{product.PageGreatingImg}" ;
            if (product == null)
                return RedirectToAction("index");

            return this.View(product);
        }

        // GET: Products/5
        public async Task<IActionResult> About()
        {
            AboutUsViewModel data = new AboutUsViewModel()
            {
                AboutUs = await _context.AboutUss.FirstOrDefaultAsync(),
                TeamMembers = await _context.TeamMembers.Where(x => x.IsViewInWebSite).OrderBy(x => x.Order ?? int.MaxValue).ToListAsync(),
                Partners = await _context.Partners.Where(x => x.IsViewInWebSite).OrderBy(x => x.Order ?? int.MaxValue).ToListAsync(),
                AboutUsTitles = await _context.AboutUsTitles.Where(x => x.IsViewInWebSite).OrderBy(x => x.Order ?? int.MaxValue).ToListAsync()
            };

            return this.View(data);
        }
        public IActionResult ContactUs()
        {
            //List<CompanyLocation> companys = _context.CompanyLocations.Where(x => x.IsViewInWebSite).ToList();
            //return this.View(companys);
            return this.View(_context.CompanyLocations.Where(x => x.IsViewInWebSite));
        }
        public IActionResult GetInTouch()
        {
            return this.View();
        }

        public IActionResult AboutCollapse()
        {

            List<AboutUsCollapsible> data = _context.AboutUsCollapsibles.Where(x => x.IsViewInWebSite).OrderBy(x => x.Order).ToList();
            return this.View(data);
        }


    }
}
