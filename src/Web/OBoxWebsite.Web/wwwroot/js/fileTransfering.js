﻿//------ Start file upload functions
function parseFileOfInputId(fileUploadPrefix, e) {
    let inputId = fileUploadPrefix + "inputId";
    let imageId = fileUploadPrefix + "imageId";

    console.log("fileUploadPrefix", fileUploadPrefix, "inputId", inputId, "imageId", imageId)
    var file = e.target.files[0];
    if (file) {
        console.log(file)
        getBase64(file, function (result) {
            console.log(result);
            $('#' + inputId).val(result);
            $('#' + imageId).attr('src', result);
        });
    }
}

function getBase64(file, FuncafterReading) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        FuncafterReading(reader.result);
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}
//------ End file upload functions
