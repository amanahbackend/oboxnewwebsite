var map = new ol.Map({
    interactions: ol.interaction.defaults({ mouseWheelZoom: false }),
    target: "map",
    controls: [],
    interactions: [],
    layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM()
        })
    ],
    view: new ol.View({
        center: ol.proj.fromLonLat([50.07, 26.09]),
        zoom: 4
    })
});


let container = document.getElementById("popup");
let content = document.getElementById("popup-content");
let closer = document.getElementById("popup-closer");

let overlay = new ol.Overlay({
    element: container,
    autoPan: true,
    autoPanAnimation: {
        duration: 250
    }
});
map.addOverlay(overlay);

// closer.onclick = function () {
//     overlay.setPosition(undefined);
//     closer.blur();
//     return false;
// };



map.on('singleclick', function (evt) {
    let f = map.forEachFeatureAtPixel(
        evt.pixel,
        function (ft, layer) { return ft; }
    );
    if (f) {
        let geometry = f.getGeometry();
        let coordinates = geometry.getCoordinates();
        content.innerHTML = `<h5>Branch: ${f.get('city')}</h5> <hr> <h6>Address: ${f.get('contact')}</h6>`
        overlay.setPosition(coordinates);

    } else {
        overlay.setPosition(undefined);
        closer.blur();
    }

});

function pushMarkersToMap() {
    const locations = [
        {
            lat: 31.205753,
            lng: 29.924526,
            city: 'Egypt',
            contact: ''
        },
        {
            lat: 29.378586,
            lng: 47.990341,
            city: `Kuwait`,
            contact: 'P.O Box : 44098, Hawally 32055'
        },
        // {
        //     lat: 23.614328,
        //     lng: 58.545284,
        //     city: 'Oman',
        //     contact: ''
        // },
        // {
        //     lat: 25.286106,
        //     lng: 51.534817,
        //     city: 'Qatar',
        //     contact: ''
        // }
    ];

    locations.forEach(location => {
        addMarker(location.lat, location.lng, location.city, location.contact);
    });
}

pushMarkersToMap();


/**
 * 
 * @param {*} lat 
 * @param {*} lng 
 * @param {*} city 
 */
function addMarker(lat, lng, city, contact) {
    var vectorLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [
                new ol.Feature({
                    city: city,
                    contact: contact,
                    geometry: new ol.geom.Point(
                        ol.proj.transform(
                            [parseFloat(lng), parseFloat(lat)],
                            "EPSG:4326",
                            "EPSG:3857"
                        )
                    )
                })
            ]
        }),
        style: new ol.style.Style({
            image: new ol.style.Icon({
                anchor: [0.5, 0.5],
                anchorXUnits: "fraction",
                anchorYUnits: "fraction",
                src: "https://image.flaticon.com/icons/png/32/149/149060.png"
            })
        })
    });

    map.addLayer(vectorLayer);
}
