$(document).ready(function () {
    var owl = $(".owl-carousel");
    owl.owlCarousel({
        items: 1,
        nav: true,
        loop: true,
        responsiveClass: true,
        margin: 10,
        merge: true,
        autoWidth: true,
        autoHeight: true,
        center: true,
        mergeFit: true,
        responsive: {
            1000: {
                items: 1,
                nav: true,
                loop: true,
                responsiveClass: true,
                margin: 10,
                merge: true,
                autoWidth: true,
                autoHeight: true,
                center: true,
                mergeFit: true
            },
            800: {
                items: 1,
                nav: true,
                loop: true,
                responsiveClass: true,
                margin: 10,
                merge: true,
                autoWidth: true,
                autoHeight: true,
                center: true,
                mergeFit: true
            },
            678: {
                items: 1,
                nav: true,
                loop: true,
                responsiveClass: true,
                margin: 10,
                merge: true,
                autoWidth: true,
                autoHeight: true,
                center: true,
                mergeFit: true,
                height: 200
            },
            300: {
                items: 1,
                nav: true,
                loop: true,
                responsiveClass: true,
                margin: 10,
                merge: true,
                autoWidth: true,
                autoHeight: true,
                center: true,
                mergeFit: true,
                height: 200
            },

            200: {
                items: 1,
                nav: true,
                loop: true,
                responsiveClass: true,
                margin: 10,
                merge: true,
                autoWidth: true,
                autoHeight: true,
                center: true,
                mergeFit: true
            },
            50: {
                items: 1,
                nav: true,
                loop: true,
                responsiveClass: true,
                margin: 10,
                merge: true,
                autoWidth: true,
                autoHeight: true,
                center: true,
                mergeFit: true
            }
        },
        navigation: true,
        navigationText: [
            '<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-chevron-circle-left fa-stack-1x fa-inverse"></i></span>',
            '<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-chevron-circle-right fa-stack-1x fa-inverse"></i></span>'
        ]
    });
});
