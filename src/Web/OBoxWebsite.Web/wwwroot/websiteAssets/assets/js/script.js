$(document).ready(function () {
    $('#main-nav .obox-nav').addClass('opacity-obox');

    // animations
    $(".dropdown-menu").mouseout(function () {
        $(".dropdown-menu div.div-item").addClass("animated bounceOutDown fast");
        $(".dropdown-menu").css("background", "none");
        $(".dropdown-menu").css("display", "none");
    });

    $(".dropdown-menu").mouseover(function () {
        $(".dropdown-menu").css("display", "none");
    });


    $(".dropdown").mouseover(function () {
        $(".dropdown-menu div.div-item").removeClass("animated bounceInDown fast");
        $(".dropdown-menu").css("background", "white");
        $(".dropdown-menu").css("display", "block");
    });

});


$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 200) {
        $("#main-nav").css('position', 'fixed');
        $('#main-nav .obox-nav').removeClass('opacity-obox');
        $("#obox-product-menu").css('position', 'fixed');
    } else if (scroll < 200) {
        // animations
        $("#obox-product-menu .menu-items").addClass("animated fadeInRightBig faster");
        $("#obox-product-menu #obox-logo").addClass("animated fadeInLeftBig faster");
        $("#main-nav").css('position', 'relative');
        $('#main-nav .obox-nav').addClass('opacity-obox');
        $("#obox-product-menu").css('position', 'relative');
    }

});



// mobile menu
$(document).ready(function ($) {
    $("#mmenu").hide();
    $(".mtoggle").click(function () {
        $("#mmenu").slideToggle(500);
    });

    //hide menu
    if ($(window).width() <= 480) {
        $('#mobile').show();
    } else {
        $('#mobile').hide();
    }
});



