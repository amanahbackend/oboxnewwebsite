/**
 * 
 * @param {*} lat 
 * @param {*} lng 
 * @param {*} city 
 */
function addMarker(lat, lng, city, contact, mapView) {
    var vectorLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [
                new ol.Feature({
                    city: city,
                    contact: contact,
                    geometry: new ol.geom.Point(
                        ol.proj.transform(
                            [parseFloat(lng), parseFloat(lat)],
                            "EPSG:4326",
                            "EPSG:3857"
                        )
                    )
                })
            ]
        }),
        style: new ol.style.Style({
            image: new ol.style.Icon({
                anchor: [0.5, 0.5],
                anchorXUnits: "fraction",
                anchorYUnits: "fraction",
                src: "https://image.flaticon.com/icons/png/32/149/149060.png"
            })
        })
    });

    mapView.addLayer(vectorLayer);
}


// var omanMap = new ol.Map({
//     interactions: ol.interaction.defaults({ mouseWheelZoom: false }),
//     target: "oman-map",
//     controls: [],
//     interactions: [],
//     layers: [
//         new ol.layer.Tile({
//             source: new ol.source.OSM()
//         })
//     ],
//     view: new ol.View({
//         center: ol.proj.fromLonLat([55.614328, 20.545284]),
//         zoom: 4
//     })
// });
// addMarker(23.614328, 58.545284, 'Oman', '', omanMap);


// var qatarMap = new ol.Map({
//     interactions: ol.interaction.defaults({ mouseWheelZoom: false }),
//     target: "qatar-map",
//     controls: [],
//     interactions: [],
//     layers: [
//         new ol.layer.Tile({
//             source: new ol.source.OSM()
//         })
//     ],
//     view: new ol.View({
//         center: ol.proj.fromLonLat([51.28, 25.534]),
//         zoom: 7
//     })
// });
// addMarker(25.286106, 51.534817, 'Qatar', '', qatarMap);
