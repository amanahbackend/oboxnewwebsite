﻿using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreMvcTemplate.Web.Helpers
{

    public interface IProductsHelper
    {
        List<Product> ViewedProducts { get; }
    }

    public class ProductsHelper : IProductsHelper
    {
        public ProductsHelper(ApplicationDbContext _context)
        {
            ViewedProducts = _context.Products.Where(x=> x.IsViewInWebSite).OrderBy(x => x.Order).ToList();
        }

        public List<Product> ViewedProducts { get; private set; }
    }

}
