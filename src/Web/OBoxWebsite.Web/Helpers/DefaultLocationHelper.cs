﻿using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreMvcTemplate.Web.Helpers
{

    public interface IDefaultLocationHelper
    {
        List<CompanyLocation> DefaultLocations { get; }
    }

    public class DefaultLocationHelper : IDefaultLocationHelper
    {
        public DefaultLocationHelper(ApplicationDbContext _context)
        {
            DefaultLocations = _context.CompanyLocations.Where(x => x.IsDefaultLocation).OrderBy(x=> x.Order).ToList();

           
        }
        public List<CompanyLocation> DefaultLocations { get; private set; }
    }

}
