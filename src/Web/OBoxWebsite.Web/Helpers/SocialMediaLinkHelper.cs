﻿using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreMvcTemplate.Web.Helpers
{

    public interface ISocialMediaLinkHelper
    {
        List<SocialMediaLink> Links { get; }
    }

    public class SocialMediaLinkHelper : ISocialMediaLinkHelper
    {
        public SocialMediaLinkHelper(ApplicationDbContext _context)
        {
            Links = _context.SocialMediaLinks.Where(x => x.IsViewInWebSite).ToList();
        }
        public List<SocialMediaLink> Links { get; private set; }
    }

}
