﻿using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreMvcTemplate.Web.Helpers
{

    public interface IQuickLinkssHelper
    {
        List<QuickLink> ViewedQuickLinks { get; }
    }

    public class QuickLinkssHelper : IQuickLinkssHelper
    {
        public QuickLinkssHelper(ApplicationDbContext _context)
        {
            ViewedQuickLinks = _context.QuickLinks.Where(x=> x.IsViewInWebSite).OrderBy(x => x.Order).ToList();
        }
        
        public List<QuickLink> ViewedQuickLinks { get; private set; }
    }

}
