﻿namespace AspNetCoreMvcTemplate.Web
{
    using AspNetCoreMvcTemplate.Services;
    using AspNetCoreMvcTemplate.Services.EmailServices;
    using AspNetCoreMvcTemplate.Services.FileService;
    using AspNetCoreMvcTemplate.Web.Helpers;
    using Common.Mapping;
    using Data;
    using Data.Models;
    using Infrastructure.Extensions;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Features;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.UI;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Swashbuckle.AspNetCore.Swagger;



    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    this.Configuration.GetConnectionString("DefaultConnection")));

            services.AddDefaultIdentity<ApplicationUser>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
            })
            .AddRoles<IdentityRole>()
            .AddDefaultUI(UIFramework.Bootstrap4)
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            // lower case urls - user friendly
            services.AddRouting(options => options.LowercaseUrls = true);

            // services
            services.AddDomainServices();

            services.AddScoped(typeof(IProductsHelper), typeof(ProductsHelper));
            services.AddScoped(typeof(IQuickLinkssHelper), typeof(QuickLinkssHelper));
            services.AddScoped(typeof(IDefaultLocationHelper), typeof(DefaultLocationHelper));
            services.AddScoped(typeof(ISocialMediaLinkHelper), typeof(SocialMediaLinkHelper));

            services.AddSingleton(typeof(IFileService), typeof(FileService));
            services.AddSingleton(typeof(IEmailService), typeof(EmailService));

            
            //services.Configure<FormOptions>(Options => Options.ValueLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.ValueCountLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.KeyLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.MemoryBufferThreshold = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.BufferBodyLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.ValueCountLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.ValueLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.MultipartBoundaryLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.MultipartHeadersCountLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.MultipartHeadersLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.MultipartBodyLengthLimit = 10*1024*1024);

            //services.Configure<FormOptions>(options =>
            //{
            //    options.MultipartBodyLengthLimit = 10 * 1024 * 1024;
            //});


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //services.Configure<FormOptions>(Options => Options.ValueLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.ValueCountLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.KeyLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.MemoryBufferThreshold = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.BufferBodyLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.ValueCountLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.ValueLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.MultipartBoundaryLengthLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.MultipartHeadersCountLimit = int.MaxValue);
            //services.Configure<FormOptions>(Options => Options.MultipartHeadersLengthLimit = int.MaxValue);
            services.Configure<FormOptions>(Options => Options.MultipartBodyLengthLimit = 52428800);

            services.Configure<FormOptions>(options =>
            {
                options.MultipartBodyLengthLimit = 52428800;
            });



            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new Info { Title = "amanah API", Version = "v1" });
            //});

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //app.UseFormWorkaroundMiddleware();
            // automapper
            InitializeAutoMapper.AddCurrentProfile();

            // adds admin and updates db
            app.UseDatabaseMigration();

         

            app.UseDeveloperExceptionPage();
            app.UseDatabaseErrorPage();




            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //    app.UseDatabaseErrorPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //    app.UseHsts();
            //}

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();



            app.UseMvc();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "areas",
                  template: "{area:exists}/{controller=AdminHome}/{action=Index}/{id?}"
                );
            });

            //app.UseFormWorkaroundMiddleware();



            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Amana razor & api");
            //});

        }
    }
}
