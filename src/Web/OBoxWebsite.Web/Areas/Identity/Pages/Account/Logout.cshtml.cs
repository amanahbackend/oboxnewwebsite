﻿namespace AspNetCoreMvcTemplate.Web.Areas.Identity.Pages.Account
{
    using Data.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    [AllowAnonymous]
    public class LogoutModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ILogger<LogoutModel> logger;

        public LogoutModel(SignInManager<ApplicationUser> signInManager, ILogger<LogoutModel> logger)
        {
            this.signInManager = signInManager;
            this.logger = logger;
        }

        public async Task OnGetAsync()
        {
            await this.OnPost();
        }

        public async Task<IActionResult> OnPost(string returnUrl = null)
        {
            returnUrl = returnUrl ?? "~/Admin";
            await this.signInManager.SignOutAsync();
            this.logger.LogInformation("User logged out.");

            //return RedirectToAction("index", "Home", new { area = "" });
            //return this.LocalRedirect(returnUrl);
            return this.RedirectToPage("./Lockout");

            //return RedirectToAction("", "Home", new { area = "" });
            //Identity/Account/login

            //return this.LocalRedirect("~");
        }
    }
}