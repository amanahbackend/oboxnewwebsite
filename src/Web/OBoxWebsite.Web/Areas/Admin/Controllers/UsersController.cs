﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace AspNetCoreMvcTemplate.Web.Controllers
{
    [Area("Admin")]
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly UserManager<ApplicationUser> userManager;


        public UsersController(
            ApplicationDbContext context,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.signInManager = signInManager; 
            this.userManager = userManager;
        }

        // GET: ImageToAdds
        public async Task<IActionResult> Index()
        {
            return View(await this.userManager.Users.ToListAsync());
        }

        // GET: ImageToAdds/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await this.userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: ImageToAdds/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ImageToAdds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IsViewInWebSite,Description,Url")] ImageToAdd imageToAdd)
        {
            if (ModelState.IsValid)
            {
                _context.Add(imageToAdd);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(imageToAdd);
        }


        // GET: ImageToAdds/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var imageToAdd = await _context.ImageToAdds.FindAsync(id);
            if (imageToAdd == null)
            {
                return NotFound();
            }
            return View(imageToAdd);
        }

        // POST: ImageToAdds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IsViewInWebSite,Description,Url")] ImageToAdd imageToAdd)
        {
            if (id != imageToAdd.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(imageToAdd);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ImageToAddExists(imageToAdd.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(imageToAdd);
        }

        // GET: ImageToAdds/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await this.userManager.FindByIdAsync(id);
             
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: ImageToAdds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await this.userManager.FindByIdAsync(id);
            await this.userManager.DeleteAsync(user);
            return RedirectToAction(nameof(Index));
        }

        private bool ImageToAddExists(int id)
        {
            return _context.ImageToAdds.Any(e => e.Id == id);
        }
    }
}
