﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;

namespace AspNetCoreMvcTemplate.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SubscriberModelsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SubscriberModelsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/SubscriberModels
        public async Task<IActionResult> Index()
        {
            return View(await _context.SubscriberModels.ToListAsync());
        }

        // GET: Admin/SubscriberModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var SubscriberModel = await _context.SubscriberModels
                .FirstOrDefaultAsync(m => m.Id == id);
            if (SubscriberModel == null)
            {
                return NotFound();
            }

            return View(SubscriberModel);
        }

     
        // GET: Admin/SubscriberModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var SubscriberModel = await _context.SubscriberModels
                .FirstOrDefaultAsync(m => m.Id == id);
            if (SubscriberModel == null)
            {
                return NotFound();
            }

            return View(SubscriberModel);
        }

        // POST: Admin/SubscriberModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var SubscriberModel = await _context.SubscriberModels.FindAsync(id);
            _context.SubscriberModels.Remove(SubscriberModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SubscriberModelExists(int id)
        {
            return _context.SubscriberModels.Any(e => e.Id == id);
        }
    }
}
