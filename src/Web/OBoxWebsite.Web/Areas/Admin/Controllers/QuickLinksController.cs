﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;

namespace AspNetCoreMvcTemplate.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class QuickLinksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public QuickLinksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/QuickLinks
        public async Task<IActionResult> Index()
        {
            return View(await _context.QuickLinks.ToListAsync());
        }

        // GET: Admin/QuickLinks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quickLink = await _context.QuickLinks
                .FirstOrDefaultAsync(m => m.Id == id);
            if (quickLink == null)
            {
                return NotFound();
            }

            return View(quickLink);
        }

        // GET: Admin/QuickLinks/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/QuickLinks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Link,Order,IsViewInWebSite")] QuickLink quickLink)
        {
            if (ModelState.IsValid)
            {
                _context.Add(quickLink);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(quickLink);
        }

        // GET: Admin/QuickLinks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quickLink = await _context.QuickLinks.FindAsync(id);
            if (quickLink == null)
            {
                return NotFound();
            }
            return View(quickLink);
        }

        // POST: Admin/QuickLinks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Link,Order,IsViewInWebSite")] QuickLink quickLink)
        {
            if (id != quickLink.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(quickLink);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuickLinkExists(quickLink.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(quickLink);
        }

        // GET: Admin/QuickLinks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quickLink = await _context.QuickLinks
                .FirstOrDefaultAsync(m => m.Id == id);
            if (quickLink == null)
            {
                return NotFound();
            }

            return View(quickLink);
        }

        // POST: Admin/QuickLinks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var quickLink = await _context.QuickLinks.FindAsync(id);
            _context.QuickLinks.Remove(quickLink);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuickLinkExists(int id)
        {
            return _context.QuickLinks.Any(e => e.Id == id);
        }
    }
}
