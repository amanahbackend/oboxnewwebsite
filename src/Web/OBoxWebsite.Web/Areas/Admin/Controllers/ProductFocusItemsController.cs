﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using AspNetCoreMvcTemplate.Services.FileService;

namespace AspNetCoreMvcTemplate.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductFocusItemsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileService _fileService;
        public ProductFocusItemsController(ApplicationDbContext context, IFileService fileService)
        {
            _context = context;
            _fileService = fileService;
        }

        // GET: Admin/ProductFocusItems
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ProductFocusItems.Include(p => p.Product);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Admin/ProductFocusItems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productFocusItem = await _context.ProductFocusItems
                .Include(p => p.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productFocusItem == null)
            {
                return NotFound();
            }

            return View(productFocusItem);
        }

        // GET: Admin/ProductFocusItems/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name");
            return View();
        }

        // POST: Admin/ProductFocusItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductId,Title,Description,Image,Order,IsViewInWebSite")] ProductFocusItem productFocusItem)
        {
            if (ModelState.IsValid)
            {
                productFocusItem.Image = _fileService.ImageUrlrl(productFocusItem.Image);
                _context.Add(productFocusItem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name", productFocusItem.ProductId);
            return View(productFocusItem);
        }

        // GET: Admin/ProductFocusItems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productFocusItem = await _context.ProductFocusItems.FindAsync(id);
            if (productFocusItem == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name", productFocusItem.ProductId);
            return View(productFocusItem);
        }

        // POST: Admin/ProductFocusItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,Title,Description,Image,Order,IsViewInWebSite")] ProductFocusItem productFocusItem)
        {
            if (id != productFocusItem.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    productFocusItem.Image = _fileService.ImageUrlrl(productFocusItem.Image);
                    _context.Update(productFocusItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductFocusItemExists(productFocusItem.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name", productFocusItem.ProductId);
            return View(productFocusItem);
        }

        // GET: Admin/ProductFocusItems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productFocusItem = await _context.ProductFocusItems
                .Include(p => p.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productFocusItem == null)
            {
                return NotFound();
            }

            return View(productFocusItem);
        }

        // POST: Admin/ProductFocusItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var productFocusItem = await _context.ProductFocusItems.FindAsync(id);
            _context.ProductFocusItems.Remove(productFocusItem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductFocusItemExists(int id)
        {
            return _context.ProductFocusItems.Any(e => e.Id == id);
        }
    }
}
