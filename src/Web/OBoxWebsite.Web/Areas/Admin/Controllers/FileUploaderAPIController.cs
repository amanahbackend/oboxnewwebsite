﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using AspNetCoreMvcTemplate.Web.Infrastructure.Extensions;
using System.IO;
using System.Net.Http;
using System.Web;
using AspNetCoreMvcTemplate.Web.ViewModels;
using AspNetCoreMvcTemplate.Services.FileService;

namespace WebApplicationWithIdentity.Controllers
{


    [Route("api/[controller]")]
    [ApiController]
    public class FileUploaderAPIController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private IHostingEnvironment _hostingEnvironment;
        private IFileService _fileService;

        

        public FileUploaderAPIController(
            ApplicationDbContext context,
            IHostingEnvironment hostingEnvironment,
            IFileService fileService
            )
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _fileService = fileService;
        }


        // GET api/values
        [HttpGet("test")]
        public async Task<IActionResult> Get() => new JsonResult("test success");






        //[HttpPost]
        //[RequestSizeLimit(5242880)]//max = 5MB
        //public IActionResult SaveImage([FromBody]Base64FileViewModel obj)
        //{
        //    return new JsonResult(_fileService.SaveFile(obj.type, obj.base64));
        //}


        public string ImageUrlrl (string base64OrUrl)
        {
            if (_fileService.IsBase64(base64OrUrl))
            {
                string url = _fileService.SaveFile("data:image/", base64OrUrl);
                return url; 
            }
            return base64OrUrl; 
        }



        //public HttpResponseMessage Post()
        //{
        //    HttpResponseMessage result = null;
        //    var httpRequest = context.Response.Request;

        //    // Check if files are available
        //    if (httpRequest.Files.Count > 0)
        //    {
        //        var files = new List<string>();

        //        // interate the files and save on the server
        //        foreach (string file in httpRequest.Files)
        //        {
        //            var postedFile = httpRequest.Files[file];
        //            var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
        //            postedFile.SaveAs(filePath);

        //            files.Add(filePath);
        //        }

        //        // return result
        //        result = Request.CreateResponse(HttpStatusCode.Created, files);
        //    }
        //    else
        //    {
        //        // return BadRequest (no file(s) available)
        //        result = Request.CreateResponse(HttpStatusCode.BadRequest);
        //    }

        //    return result;
        //}












        //[HttpPost("Upload")]
        //public async Task<IActionResult> UploadMedicalReport(IFormFile File)
        //{




        //        var fileName = File.FileName.Split(".")[0] + "-" + DateTime.UtcNow.ToString("yyyy-dd-M--HH-mm-ss") + Path.GetExtension(File.FileName);
        //    fileName = Regex.Replace(fileName, @"\s+", "");

        //    var path = Path.Combine(_hostingEnvironment.WebRootPath, File.FilesUploadLocations());
        //    if (!File.IsImage())
        //    {
        //        ModelState.AddModelError("file", "this is not a valid file type");
        //        return BadRequest(ModelState);
        //    }
        //    if (File.Length > 10 * 1024 * 1024)
        //    {
        //        ModelState.AddModelError("file", "the file size must be less than 10 mb");
        //        return BadRequest(ModelState);
        //    }
        //    if (File.Length > 0)
        //    {
        //        var filePath = Path.Combine(path, fileName);
        //        using (var fileStream = new FileStream(filePath, FileMode.Create))
        //        {
        //            await File.CopyToAsync(fileStream);
        //        }
        //    }

        //    return Ok(new { FileUrl = $"{File.FilesUploadLocations()}/" + fileName });
        //}

    }
}
