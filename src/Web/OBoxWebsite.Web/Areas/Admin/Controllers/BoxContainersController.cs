﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using AspNetCoreMvcTemplate.Services.FileService;

namespace AspNetCoreMvcTemplate.Web.Controllers
{
    [Area("Admin")]
    public class BoxContainersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileService _fileService;

        public BoxContainersController(ApplicationDbContext context, IFileService fileService)
        {
            _context = context;
            _fileService = fileService; 
        }

        // GET: BoxContainers
        public async Task<IActionResult> Index()
        {
            return View(await _context.BoxContainers.ToListAsync());
        }

        // GET: BoxContainers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var boxContainer = await _context.BoxContainers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (boxContainer == null)
            {
                return NotFound();
            }

            return View(boxContainer);
        }

        // GET: BoxContainers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BoxContainers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IsViewInWebSite,Title,SubTitle,Image,Description,HasDivOnBackGround,ReadMoreUrl,Order")] BoxContainer boxContainer)
        {
            if (ModelState.IsValid)
            {
                boxContainer.Image = _fileService.ImageUrlrl(boxContainer.Image);

                _context.Add(boxContainer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(boxContainer);
        }

        // GET: BoxContainers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var boxContainer = await _context.BoxContainers.FindAsync(id);
            if (boxContainer == null)
            {
                return NotFound();
            }
            return View(boxContainer);
        }

        // POST: BoxContainers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IsViewInWebSite,Title,SubTitle,Image,Description,HasDivOnBackGround,ReadMoreUrl,Order")] BoxContainer boxContainer)
        {
            if (id != boxContainer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
            boxContainer.Image = _fileService.ImageUrlrl(boxContainer.Image);
                    _context.Update(boxContainer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BoxContainerExists(boxContainer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(boxContainer);
        }

        // GET: BoxContainers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var boxContainer = await _context.BoxContainers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (boxContainer == null)
            {
                return NotFound();
            }

            return View(boxContainer);
        }

        // POST: BoxContainers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var boxContainer = await _context.BoxContainers.FindAsync(id);
            _context.BoxContainers.Remove(boxContainer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BoxContainerExists(int id)
        {
            return _context.BoxContainers.Any(e => e.Id == id);
        }
    }
}
