﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using AspNetCoreMvcTemplate.Services.FileService;

namespace AspNetCoreMvcTemplate.Web.Controllers
{
    [Area("Admin")]
    public class SocialMediaLinksController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileService _fileService;

        public SocialMediaLinksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SocialMediaLInks
        public async Task<IActionResult> Index()
        {
            return View(await _context.SocialMediaLinks.ToListAsync());
        }

        // GET: SocialMediaLInks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var socialMediaLInk = await _context.SocialMediaLinks
                .FirstOrDefaultAsync(m => m.Id == id);
            if (socialMediaLInk == null)
            {
                return NotFound();
            }

            return View(socialMediaLInk);
        }

        // GET: SocialMediaLInks/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SocialMediaLInks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Icon,Link,IsFollowUsLink,IsViewInWebSite,Order")] SocialMediaLink socialMediaLInk)
        {
            if (ModelState.IsValid)
            {

                _context.Add(socialMediaLInk);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(socialMediaLInk);
        }

        // GET: SocialMediaLInks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var socialMediaLInk = await _context.SocialMediaLinks.FindAsync(id);
            if (socialMediaLInk == null)
            {
                return NotFound();
            }
            return View(socialMediaLInk);
        }

        // POST: SocialMediaLInks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Icon,Link,IsFollowUsLink,IsViewInWebSite,Order")] SocialMediaLink socialMediaLInk)
        {
            if (id != socialMediaLInk.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(socialMediaLInk);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SocialMediaLInkExists(socialMediaLInk.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(socialMediaLInk);
        }

        // GET: SocialMediaLInks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var socialMediaLInk = await _context.SocialMediaLinks
                .FirstOrDefaultAsync(m => m.Id == id);
            if (socialMediaLInk == null)
            {
                return NotFound();
            }

            return View(socialMediaLInk);
        }

        // POST: SocialMediaLInks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var socialMediaLInk = await _context.SocialMediaLinks.FindAsync(id);
            _context.SocialMediaLinks.Remove(socialMediaLInk);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SocialMediaLInkExists(int id)
        {
            return _context.SocialMediaLinks.Any(e => e.Id == id);
        }
    }
}
