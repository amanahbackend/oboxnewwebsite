﻿namespace AspNetCoreMvcTemplate.Web.Controllers
{
    using AspNetCoreMvcTemplate.Data.Models;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using ViewModels;

 // SignInManager<ApplicationUser> SignInManager
 //UserManager<ApplicationUser> UserManager


    [Area("Admin")]
    public class AdminHomeController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;

        public AdminHomeController(UserManager<ApplicationUser> userManager,SignInManager<ApplicationUser> signInManager)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;

        }
        public IActionResult Index()
        {
            var user =  this.userManager.GetUserAsync(User).Result;
            if (user == null)
            {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }
            return RedirectToAction("Index", "Products");
        }


        public async Task<IActionResult> LogoutAsync()
        {
            await this.signInManager.SignOutAsync();
            return RedirectToPage("/Account/Login", new { area = "Identity" });
        }



        public IActionResult Privacy()
        {
            return this.View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
