﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;

namespace AspNetCoreMvcTemplate.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SubscribersMailsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SubscribersMailsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/SubscribersMails
        public async Task<IActionResult> Index()
        {
            return View(await _context.MailToSubscribers.ToListAsync());
        }

        // GET: Admin/SubscribersMails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscribersMail = await _context.MailToSubscribers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (subscribersMail == null)
            {
                return NotFound();
            }

            return View(subscribersMail);
        }

        // GET: Admin/SubscribersMails/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/SubscribersMails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,SentDate,Subject,Body,Title,SubTitle")] SubscribersMail subscribersMail)
        {
            if (ModelState.IsValid)
            {
                _context.Add(subscribersMail);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(subscribersMail);
        }

        // GET: Admin/SubscribersMails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscribersMail = await _context.MailToSubscribers.FindAsync(id);
            if (subscribersMail == null)
            {
                return NotFound();
            }
            return View(subscribersMail);
        }

        // POST: Admin/SubscribersMails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SentDate,Subject,Body,Title,SubTitle")] SubscribersMail subscribersMail)
        {
            if (id != subscribersMail.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(subscribersMail);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubscribersMailExists(subscribersMail.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(subscribersMail);
        }

        // GET: Admin/SubscribersMails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscribersMail = await _context.MailToSubscribers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (subscribersMail == null)
            {
                return NotFound();
            }

            return View(subscribersMail);
        }

        // POST: Admin/SubscribersMails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var subscribersMail = await _context.MailToSubscribers.FindAsync(id);
            _context.MailToSubscribers.Remove(subscribersMail);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SubscribersMailExists(int id)
        {
            return _context.MailToSubscribers.Any(e => e.Id == id);
        }
    }
}
