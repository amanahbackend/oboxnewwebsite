﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;

namespace AspNetCoreMvcTemplate.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductHintsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductHintsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/ProductHints
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ProductHints.Include(p => p.Product);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Admin/ProductHints/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productHint = await _context.ProductHints
                .Include(p => p.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productHint == null)
            {
                return NotFound();
            }

            return View(productHint);
        }

        // GET: Admin/ProductHints/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name");
            return View();
        }

        // POST: Admin/ProductHints/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductId,Image,Html,IsViewInWebSite")] ProductHint productHint)
        {
            if (ModelState.IsValid)
            {
                _context.Add(productHint);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name", productHint.ProductId);
            return View(productHint);
        }

        // GET: Admin/ProductHints/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productHint = await _context.ProductHints.FindAsync(id);
            if (productHint == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name", productHint.ProductId);
            return View(productHint);
        }

        // POST: Admin/ProductHints/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,Image,Html,IsViewInWebSite")] ProductHint productHint)
        {
            if (id != productHint.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productHint);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductHintExists(productHint.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Name", productHint.ProductId);
            return View(productHint);
        }

        // GET: Admin/ProductHints/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productHint = await _context.ProductHints
                .Include(p => p.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productHint == null)
            {
                return NotFound();
            }

            return View(productHint);
        }

        // POST: Admin/ProductHints/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var productHint = await _context.ProductHints.FindAsync(id);
            _context.ProductHints.Remove(productHint);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductHintExists(int id)
        {
            return _context.ProductHints.Any(e => e.Id == id);
        }
    }
}
