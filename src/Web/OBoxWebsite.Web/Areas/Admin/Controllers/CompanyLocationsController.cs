﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;

namespace AspNetCoreMvcTemplate.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CompanyLocationsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CompanyLocationsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/CompanyLocations
        public async Task<IActionResult> Index()
        {
            return View(await _context.CompanyLocations.ToListAsync());
        }

        // GET: Admin/CompanyLocations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyLocation = await _context.CompanyLocations
                .FirstOrDefaultAsync(m => m.Id == id);
            if (companyLocation == null)
            {
                return NotFound();
            }

            return View(companyLocation);
        }

        // GET: Admin/CompanyLocations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/CompanyLocations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,LocationName,LocationAddress,Tel,Fax,ForSalesPhones,ForServiceAndSupportPhones,MapDivId,ScriptForMap,IsViewInWebSite,IsDefaultLocation,Email")] CompanyLocation companyLocation)
        {
            if (ModelState.IsValid)
            {

                //if (companyLocation.IsDefaultLocation)
                //{
                //    List<CompanyLocation> lastDefaultLocations = _context.CompanyLocations.Where(x => x.IsDefaultLocation).ToList();
                //    foreach (CompanyLocation defaultCompanyLocations in lastDefaultLocations)
                //        defaultCompanyLocations.IsDefaultLocation = false;
                //}
                _context.Add(companyLocation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(companyLocation);
        }

        // GET: Admin/CompanyLocations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyLocation = await _context.CompanyLocations.FindAsync(id);
            if (companyLocation == null)
            {
                return NotFound();
            }
            return View(companyLocation);
        }

        // POST: Admin/CompanyLocations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,LocationName,LocationAddress,Tel,Fax,ForSalesPhones,ForServiceAndSupportPhones,MapDivId,ScriptForMap,IsViewInWebSite,IsDefaultLocation,Email")] CompanyLocation companyLocation)
        {
            if (id != companyLocation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //if (companyLocation.IsDefaultLocation)
                    //{
                    //    List<CompanyLocation> lastDefaultLocations = _context.CompanyLocations.Where(x => x.IsDefaultLocation && x.Id != companyLocation.Id).ToList();
                    //    foreach (CompanyLocation defaultCompanyLocations in lastDefaultLocations)
                    //        defaultCompanyLocations.IsDefaultLocation = false;
                    //}


                    _context.Update(companyLocation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyLocationExists(companyLocation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(companyLocation);
        }

        // GET: Admin/CompanyLocations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyLocation = await _context.CompanyLocations
                .FirstOrDefaultAsync(m => m.Id == id);
            if (companyLocation == null)
            {
                return NotFound();
            }

            return View(companyLocation);
        }

        // POST: Admin/CompanyLocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var companyLocation = await _context.CompanyLocations.FindAsync(id);
            _context.CompanyLocations.Remove(companyLocation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompanyLocationExists(int id)
        {
            return _context.CompanyLocations.Any(e => e.Id == id);
        }
    }
}
