﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using AspNetCoreMvcTemplate.Services.FileService;

namespace AspNetCoreMvcTemplate.Web.Controllers
{
    [Area("Admin")]
    public class ProductsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IFileService _fileService;
        public ProductsController(ApplicationDbContext context,
            IFileService fileService)
        {
            _context = context;
            _fileService = fileService;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            return View(await _context.Products.ToListAsync());
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,PageLastSectionHTML,PageLastSectionImage,IsViewInWebSite,PageGreatingImg,PageGreatingHTML,PageFocusHeaderHTML,Description,UrPageIconl,Name,Icon,CssClass,Order,ViewInWebSiteProductFooterHTML,ProductFooterHTML")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.PageGreatingImg = _fileService.ImageUrlrl(product.PageGreatingImg);
                product.PageLastSectionImage = _fileService.ImageUrlrl(product.PageLastSectionImage);
                product.Icon = _fileService.ImageUrlrl(product.Icon);
                product.PageIcon = _fileService.ImageUrlrl(product.PageIcon);

                _context.Add(product);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,PageLastSectionHTML,PageLastSectionImage,IsViewInWebSite,PageGreatingImg,PageGreatingHTML,PageFocusHeaderHTML,Description,PageIcon,Name,Icon,CssClass,Order,ViewInWebSiteProductFooterHTML,ProductFooterHTML")] Product product)
        {
            if (id != product.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    product.PageGreatingImg = _fileService.ImageUrlrl(product.PageGreatingImg);
                    product.PageLastSectionImage = _fileService.ImageUrlrl(product.PageLastSectionImage);
                    product.Icon = _fileService.ImageUrlrl(product.Icon);
                    product.PageIcon = _fileService.ImageUrlrl(product.PageIcon);

                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _context.Products.FindAsync(id);
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.Id == id);
        }



    }
}
