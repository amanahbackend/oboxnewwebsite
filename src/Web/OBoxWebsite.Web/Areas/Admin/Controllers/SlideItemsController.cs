﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using AspNetCoreMvcTemplate.Services.FileService;

namespace AspNetCoreMvcTemplate.Web.Controllers
{

    [Area("Admin")]
    public class SlideItemsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileService _fileService;

        public SlideItemsController(ApplicationDbContext context, IFileService fileService)
        {
            _context = context;
            _fileService = fileService;

        }

        // GET: SlideItems
        public async Task<IActionResult> Index()
        {
            return View(await _context.SlideItems.ToListAsync());
        }

        // GET: SlideItems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slideItem = await _context.SlideItems
                .FirstOrDefaultAsync(m => m.Id == id);
            if (slideItem == null)
            {
                return NotFound();
            }

            return View(slideItem);
        }

        // GET: SlideItems/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SlideItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequestFormLimits(KeyLengthLimit = int.MaxValue,

             BufferBodyLengthLimit = long.MaxValue,
ValueCountLimit = int.MaxValue,
ValueLengthLimit = int.MaxValue,
MultipartBoundaryLengthLimit = int.MaxValue,
MultipartHeadersCountLimit = int.MaxValue,
MultipartHeadersLengthLimit = int.MaxValue,
 MultipartBodyLengthLimit = long.MaxValue

            )]


        //[RequestSizeLimit(10 * 1024 * 1024)]
        //[RequestSizeLimit(2147483648)]
        //[RequestSizeLimit(2147483648/1024)]
        //[DisableRequestSizeLimit]
        public async Task<IActionResult> Create([Bind("Id,Title,Order,IsViewInWebSite,SubTitle,Image,Description,HasDivOnBackGround,IsVideo,BackGroundUrl,Order")] SlideItem slideItem)
        {
            if (ModelState.IsValid)
            {
                slideItem.Image = _fileService.ImageUrlrl(slideItem.Image);
                slideItem.BackGroundUrl = _fileService.ImageUrlrl(slideItem.BackGroundUrl);
                _context.Add(slideItem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(slideItem);
        }

        // GET: SlideItems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slideItem = await _context.SlideItems.FindAsync(id);
            if (slideItem == null)
            {
                return NotFound();
            }
            return View(slideItem);
        }

        // POST: SlideItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[RequestFormLimits(long.MaxValue)]
        //[RequestFormLimits(KeyLengthLimit = int.MaxValue)]
        //[RequestFormSizeLimit(valueCountLimit: 10000, Order = 1)]

        [RequestSizeLimit(5242880*2)]//max = 5MB
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Order,IsViewInWebSite,SubTitle,Image,Description,HasDivOnBackGround,IsVideo,BackGroundUrl,Order")] SlideItem slideItem)
        {
            if (id != slideItem.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    slideItem.Image = _fileService.ImageUrlrl(slideItem.Image);
                    slideItem.BackGroundUrl = _fileService.ImageUrlrl(slideItem.BackGroundUrl);
                    _context.Update(slideItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SlideItemExists(slideItem.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(slideItem);
        }

        // GET: SlideItems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slideItem = await _context.SlideItems
                .FirstOrDefaultAsync(m => m.Id == id);
            if (slideItem == null)
            {
                return NotFound();
            }

            return View(slideItem);
        }

        // POST: SlideItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var slideItem = await _context.SlideItems.FindAsync(id);
            _context.SlideItems.Remove(slideItem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SlideItemExists(int id)
        {
            return _context.SlideItems.Any(e => e.Id == id);
        }
    }
}
