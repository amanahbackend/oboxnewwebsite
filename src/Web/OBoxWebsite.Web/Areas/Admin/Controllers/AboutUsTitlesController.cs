﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using AspNetCoreMvcTemplate.Services.FileService;

namespace AspNetCoreMvcTemplate.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AboutUsTitlesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileService _fileService;
        
        public AboutUsTitlesController(ApplicationDbContext context, IFileService fileService)
        {
            _context = context;
            _fileService = fileService;
        }

        // GET: Admin/AboutUsTitles
        public async Task<IActionResult> Index()
        {
            return View(await _context.AboutUsTitles.ToListAsync());
        }

        // GET: Admin/AboutUsTitles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutUsTitle = await _context.AboutUsTitles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aboutUsTitle == null)
            {
                return NotFound();
            }

            return View(aboutUsTitle);
        }

        // GET: Admin/AboutUsTitles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/AboutUsTitles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Order,Image,Description,IsViewInWebSite")] AboutUsTitle aboutUsTitle)
        {
            if (ModelState.IsValid)
            {
                aboutUsTitle.Image = _fileService.ImageUrlrl(aboutUsTitle.Image);
                _context.Add(aboutUsTitle);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(aboutUsTitle);
        }

        // GET: Admin/AboutUsTitles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutUsTitle = await _context.AboutUsTitles.FindAsync(id);
            if (aboutUsTitle == null)
            {
                return NotFound();
            }
            return View(aboutUsTitle);
        }

        // POST: Admin/AboutUsTitles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Order,Image,Description,IsViewInWebSite")] AboutUsTitle aboutUsTitle)
        {
            if (id != aboutUsTitle.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    aboutUsTitle.Image = _fileService.ImageUrlrl(aboutUsTitle.Image);

                    _context.Update(aboutUsTitle);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AboutUsTitleExists(aboutUsTitle.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(aboutUsTitle);
        }

        // GET: Admin/AboutUsTitles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutUsTitle = await _context.AboutUsTitles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aboutUsTitle == null)
            {
                return NotFound();
            }

            return View(aboutUsTitle);
        }

        // POST: Admin/AboutUsTitles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var aboutUsTitle = await _context.AboutUsTitles.FindAsync(id);
            _context.AboutUsTitles.Remove(aboutUsTitle);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AboutUsTitleExists(int id)
        {
            return _context.AboutUsTitles.Any(e => e.Id == id);
        }
    }
}
