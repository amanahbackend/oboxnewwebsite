﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;

namespace OBoxWebsite.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AboutUsCollapsiblesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AboutUsCollapsiblesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/AboutUsCollapsibles
        public async Task<IActionResult> Index()
        {
            return View(await _context.AboutUsCollapsibles.ToListAsync());
        }

        // GET: Admin/AboutUsCollapsibles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutUsCollapsible = await _context.AboutUsCollapsibles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aboutUsCollapsible == null)
            {
                return NotFound();
            }

            return View(aboutUsCollapsible);
        }

        // GET: Admin/AboutUsCollapsibles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/AboutUsCollapsibles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,Order,IsViewInWebSite")] AboutUsCollapsible aboutUsCollapsible)
        {
            if (ModelState.IsValid)
            {
                _context.Add(aboutUsCollapsible);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(aboutUsCollapsible);
        }

        // GET: Admin/AboutUsCollapsibles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutUsCollapsible = await _context.AboutUsCollapsibles.FindAsync(id);
            if (aboutUsCollapsible == null)
            {
                return NotFound();
            }
            return View(aboutUsCollapsible);
        }

        // POST: Admin/AboutUsCollapsibles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,Order,IsViewInWebSite")] AboutUsCollapsible aboutUsCollapsible)
        {
            if (id != aboutUsCollapsible.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(aboutUsCollapsible);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AboutUsCollapsibleExists(aboutUsCollapsible.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(aboutUsCollapsible);
        }

        // GET: Admin/AboutUsCollapsibles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aboutUsCollapsible = await _context.AboutUsCollapsibles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aboutUsCollapsible == null)
            {
                return NotFound();
            }

            return View(aboutUsCollapsible);
        }

        // POST: Admin/AboutUsCollapsibles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var aboutUsCollapsible = await _context.AboutUsCollapsibles.FindAsync(id);
            _context.AboutUsCollapsibles.Remove(aboutUsCollapsible);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AboutUsCollapsibleExists(int id)
        {
            return _context.AboutUsCollapsibles.Any(e => e.Id == id);
        }
    }
}
