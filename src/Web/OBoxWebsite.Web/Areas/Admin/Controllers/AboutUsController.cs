﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCoreMvcTemplate.Data;
using AspNetCoreMvcTemplate.Data.Models;
using AspNetCoreMvcTemplate.Services.FileService;

namespace AspNetCoreMvcTemplate.Web.Controllers
{
    [Area("Admin")]
    public class AboutUsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileService _fileService; 
        public AboutUsController(ApplicationDbContext context, IFileService fileService)
        {
            _context = context;
            _fileService = fileService;
        }


        // GET: AboutUs/Edit/5
        public async Task<IActionResult> Edit()
        {
            AboutUs AboutUs = await _context.AboutUss.FirstOrDefaultAsync();
            if (AboutUs == null)
            {
                AboutUs = new AboutUs()
                {
                    PageGreatingImg = "",
                    PageGreatingHTML = "",
                    PageLastSectionHTML = "",
                    PageLastSectionImage = ""
                };
                await _context.AboutUss.AddAsync(AboutUs);
                await _context.SaveChangesAsync();
            }
            return View(AboutUs);
        }
        // POST: AboutUs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,PageGreatingImg,PageGreatingHTML,PageLastSectionHTML,PageLastSectionImage")] AboutUs AboutUs)
        {
            AboutUs dbModel = await _context.AboutUss.FirstOrDefaultAsync();
            dbModel.PageGreatingImg = _fileService.ImageUrlrl(AboutUs.PageGreatingImg);
            dbModel.PageGreatingHTML = AboutUs.PageGreatingHTML;
            dbModel.PageLastSectionHTML = AboutUs.PageLastSectionHTML;
            dbModel.PageLastSectionImage = _fileService.ImageUrlrl(AboutUs.PageLastSectionImage); 

            if (ModelState.IsValid)
            {
                try
                {

                    _context.Update(dbModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AboutUsExists(AboutUs.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Edit));
            }
            return View(AboutUs);
        }

        private bool AboutUsExists(int id)
        {
            return _context.AboutUss.Any(e => e.Id == id);
        }
    }
}
