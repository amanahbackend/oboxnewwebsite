﻿using AspNetCoreMvcTemplate.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreMvcTemplate.Web.ViewModels
{
    public class IndexVM
    {
        public List<ImageToAdd> ImageToAdds { get; set; }
        public List<Product> Products { get; set; }
        public List<SlideItem> SlideItems { get; set; }
        public List<BoxContainer> BoxContainers { get; set; }
        public List<Partner> Partners { get; set; }
    }
}
