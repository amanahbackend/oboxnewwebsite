﻿using AspNetCoreMvcTemplate.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreMvcTemplate.Web.ViewModels
{
    public class AboutUsViewModel
    {
        public AboutUs AboutUs { get; set; }
        public List<TeamMember> TeamMembers { get; set; }
        public List<Partner> Partners { get; set; }
        public List<AboutUsTitle> AboutUsTitles { get; set; }
    }
}
