﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreMvcTemplate.Web.ViewModels
{
    public class SubscriberViewModel
    {
        public string Email { get; set; }
    }
}
